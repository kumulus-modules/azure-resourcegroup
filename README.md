# azure-resourcegroup

## Getting started

This module aims to create a Azure Resource Group.


## Using this module

To use this module, the following variables must be filled.

Example
name = rg-example
location = brazilsoutheast
tags = {
    project = "project X"
    environment = "dev"
}
