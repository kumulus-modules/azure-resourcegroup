output "object" {
  description = "Returns the full set of resource group objects created"
  depends_on = [azurerm_resource_group.rg]
  value = azurerm_resource_group.rg
}

output "name" {
  description = "Returns a map of resource_group key -> resource_group name"
  depends_on = [azurerm_resource_group.rg]
  value      = azurerm_resource_group.rg.name
}

output "ids" {
  description = "Returns a map of resource_group key -> resource_group id"
  depends_on = [azurerm_resource_group.rg]
  value      = azurerm_resource_group.rg.id
}
