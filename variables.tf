
variable "name" {
  type        = string
  description = "Name of the resource group"
}

variable "location" {
  type        = string
  description = "Location where resource will be created"
}

variable "tags" {
  description = "(Required) tags for the deployment"
}
